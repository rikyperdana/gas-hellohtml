m.render document.body, m \.content,
	m \h4, 'Table Sample'
	m \table.bordered,
		m \thead, m \tr, [til 4]map ->
			m \th, "Column #{it+1}"
		m \tbody, [til 5]map (i) -> m \tr,
			[til 4]map (j) -> m \td, "cell #{i+1},#{j+1}"